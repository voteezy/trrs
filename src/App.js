import React from 'react';
import './App.css';
import ProjectPage from './pages/ProjectPage.js';
import ProjectsPage from './pages/ProjectsPage.js';
import CreateProjectForm from './components/CreateProjectForm.js';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import "semantic-ui-css/semantic.min.css";
import { Container, Menu, Header } from 'semantic-ui-react';

function App() {
  return (
    <Router>
      <TrrsMenu />
      {/*
        A <Switch> looks through all its children <Route>
        elements and renders the first one whose path
        matches the current URL. Use a <Switch> any time
        you have multiple routes, but you want only one
        of them to render at a time
      */}
      <Switch>
        <Route exact path="/">
          <Welcome />
        </Route>
        <Route path="/projects/:id">
          <ProjectPage />
        </Route>
        <Route path="/projects">
          <ProjectsPage />
        </Route>
        <Route path="/add-project">
          <CreateProjectForm />
        </Route>
      </Switch>
    </Router>
  );
}

function Welcome() {
  return (
    <Container text style={{ marginTop: '7em' }}>
      <Header as='h1'>Welcome to the TRRS application</Header>
    </Container>
  )
}

function TrrsMenu() {
  return (
    <Menu fixed='top'>
      <Container>
        <Menu.Item as='a' header>
          TRRS
        </Menu.Item>
        <Menu.Item as='a' href="/">Home</Menu.Item>
        <Menu.Item as='a' href="/projects">Projects</Menu.Item>
        <Menu.Item as='a' href="/add-project">Add Project</Menu.Item>
      </Container>
    </Menu>
  )
}

export default App;
