import React from 'react';
import { Container, Header } from 'semantic-ui-react';
import ProjectCollection from '../components/ProjectCollection.js';

export default function ProjectsPage() {
  return (
    <Container text style={{ marginTop: '7em' }}>
      <Header as='h1'>Select a Project</Header>
      <ProjectCollection />
    </Container>
  )
}
