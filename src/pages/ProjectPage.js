import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {
  useParams
} from "react-router-dom";
import SelfAssessmentForm from '../components/SelfAssessmentForm.js';
import RiskProfile from '../components/RiskProfile.js';
import { Container, Header, Tab, Message } from 'semantic-ui-react';

export default function ProjectPage() {

  let { id } = useParams();
  const [ project, setProject ] = useState({});

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(`http://108.61.23.159/projects/${id}`);
      setProject(result.data);
    }
    fetchData();
  }, [ id ]);

  if ( !project ) return null;

  const panes = [
    { menuItem: 'Risk Profile', render: () => <Tab.Pane><RiskProfile projectId={id} /></Tab.Pane> },
    { menuItem: 'Reviews', render: () => <Tab.Pane>Tab 2 Content</Tab.Pane> },
    { menuItem: 'Self-Assessments', render: () => <Tab.Pane><SelfAssessmentForm projectId={id} /></Tab.Pane> },
  ]
  
  const TabExampleBasic = () => <Tab panes={panes} />

  const { name } = project;
  
  return (
  <Container text style={{ marginTop: '7em' }}>
    <Header as='h1'>{name}</Header>
    <Message warning>
      <Message.Header>Attention: Self-Assessment Due</Message.Header>
      <p>Immediate action is required. Please submit a Self-Assessment for <strong>{name}</strong>.</p>
    </Message>
    <TabExampleBasic />
  </Container>
  );
}

