import React from 'react';
import { Card } from 'semantic-ui-react';
import moment from 'moment';

export default function ProjectCard ( props ) {
  console.log( 'props', props );
  const { id, name, created_at, description } = props;

  console.log( 'id', id );

  return (
    <Card
      href={`/projects/${id}`}
      header={name}
      meta={`Added ${moment(created_at).fromNow()}`}
      description={description}
    >
    </Card>
  );
}
