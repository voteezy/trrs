import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamation } from '@fortawesome/free-solid-svg-icons';
import { Label, Table } from 'semantic-ui-react';
import moment from 'moment';

export default function RiskProfile( props ) {

  const { projectId } = props;

  const [ assessments, setAssessments ] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(`http://108.61.23.159/self-assessments?project=${projectId}`);
      setAssessments(result.data);
    }
    fetchData();
  }, [ projectId ]);

  if ( !assessments ) return null;

  const assessmentRows = assessments.map( a => <AssessmentRow {...a} /> )

  return (
    <Table celled collapsing compact>
      <Table.Header>
        <Table.Row textAlign="center">
          <Table.HeaderCell />
          <Table.HeaderCell>Technical</Table.HeaderCell>
          <Table.HeaderCell>Project Management</Table.HeaderCell>
          <Table.HeaderCell>Financial Management</Table.HeaderCell>
          <Table.HeaderCell>Scope Management</Table.HeaderCell>
          <Table.HeaderCell>Resources</Table.HeaderCell>
          <Table.HeaderCell>Customer Satisfaction</Table.HeaderCell>
          <Table.HeaderCell>Security</Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {assessmentRows}
      </Table.Body>
    </Table>
  );
}

function AssessmentRow( props ) {
  const { created_at, attachments } = props;

  const riskMetrics = [
    'technical',
    'projectManagement',
    'financialManagement',
    'scopeManagement',
    'resources',
    'customerSatisfaction',
    'security'
  ];

  const riskCells = riskMetrics.map( r => {
    const riskCell = props[r] === 'Low' ?
        <Table.Cell positive>
          Low
        </Table.Cell> :
      props[r] === 'Medium' ?
        <Table.Cell warning>
          <Label color="brown">Medium</Label>
        </Table.Cell> :
        <Table.Cell negative>
          <Label color="red"><FontAwesomeIcon icon={faExclamation} />&nbsp;High</Label>
        </Table.Cell>;

    return riskCell;
  } );

  return (
    <Table.Row textAlign="center">
      <Table.Cell>
        {moment(created_at).fromNow()}
        { attachments && attachments.length >= 1 &&
          <Label as='a' href={`http://108.61.23.159${attachments[0].url}`}>
            {/* <Icon name='mail' /> */}
            {attachments[0].name}
          </Label>
        }
      </Table.Cell>
      {riskCells}
    </Table.Row>
  );
}